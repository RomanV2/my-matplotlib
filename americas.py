import pygal


# создаем экземпляр класса World
wm = pygal.maps.world.World()
wm.title = 'Северная, Центральная и Южная Америка'

wm.add('Северная Америка', ['ca', 'mx', 'us'])
wm.add('Центальная Америка', ['bz', 'cr', 'gt', 'hn', 'ni', 'pa', 'sv'])
wm.add('Южная Америка', ['ar', 'bo', 'br', 'cl', 'co', 'ec', 'gf', 'gy', 'pe', 'py', 'sr', 'uy', 've'])

wm.render_to_file('result/americas.svg')
