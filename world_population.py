import json
import pygal
from pygal.style import RotateStyle

from country_codes import get_country_code

# список заполняется данными
filename = 'data/population_data.json'
with open(filename) as f:
    pop_data = json.load(f)

cc_populations = {}

# вывод населения каждой страны за 2010 год
for pop_dict in pop_data:
    if pop_dict['Year'] == '2010':
        country_name = pop_dict['Country Name']
        population = int(float(pop_dict['Value']))
        code = get_country_code(country_name)
        if code:
            cc_populations[code] = population

# группировка стран по 3 уровням населения
cc_pops1, cc_pops2, cc_pops3 = {}, {}, {}
for cc, pop in cc_populations.items():
    if pop < 10_000_000:
        cc_pops1[cc] = pop
    elif pop < 1_000_000_000:
        cc_pops2[cc] = pop
    else:
        cc_pops3[cc] = pop

# проверка количества стран на каждом уровне
print(len(cc_pops1), len(cc_pops2), len(cc_pops3))

# создаем экземпляр класса World
wm = pygal.maps.world.World()
wm.title = 'Население мира за 2010 г'
wm.add('0-10m', cc_pops1)
wm.add('10m-1bn', cc_pops2)
wm.add('>1bn', cc_pops3)

wm.render_to_file('result/world_populations.svg')
