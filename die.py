from random import randint

class Die:
    """
    Класс кубика
    """

    def __init__(self, num_sides=6):
        """
        по умолчанию 6 граней
        """
        self.num_sides = num_sides

    def roll(self) -> int:
        """
        Взвращает случанойное число от 1 до числа граней
        :return: int
        """
        return randint(1, self.num_sides)

