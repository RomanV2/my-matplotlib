from pygal_maps_world import i18n


# выводим код страны и ее название в принятом международном формате
for country_code in sorted(i18n.COUNTRIES.keys()):
    print(f"{country_code} - {i18n.COUNTRIES[country_code]}")
