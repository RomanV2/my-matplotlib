import unittest

from employee import Employee


class TestEmployee(unittest.TestCase):
    """тесты файла 'employee.py'."""
    def setUp(self) -> None:
        """настройки тестов"""
        salary = 200_000
        self.sole_employee = Employee(salary)
        self.up = ['', 50_000]

    def test_default_raise(self):
        """тест повышения по умолчанию нормально работает?"""
        self.sole_employee.give_raise()
        self.assertEqual(self.sole_employee.show_salary(), 220_000)

    def test_custom_raise(self):
        """тест повышения на 50_000 нормально работает?"""
        self.sole_employee.give_raise(50_000)
        self.assertEqual(self.sole_employee.show_salary(), 250_000)


if __name__ == '__main__':
    unittest.main()
