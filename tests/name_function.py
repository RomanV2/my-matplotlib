def get_formatted_name(first, last, middle=''):
    """
    Строит отформатированное полное имя

    :param str first: имя
    :param str middle: отчество
    :param str last: фамилия
    :return str: ФИО
    """
    if middle:
        full_name = f"{first} {middle} {last}"
    else:
        full_name = f"{first} {last}"
    return full_name.title()


print(get_formatted_name('джонни', 'деп'))
print(get_formatted_name('василий', 'пупкин', 'петрович'))

