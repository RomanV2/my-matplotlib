import unittest

from survey import AnonymousSurvey as AnS


class TestAnonymousSurvey(unittest.TestCase):
    """тесты для класса 'survey.py'"""

    def setUp(self) -> None:
        """создание опроса и набора ответов для всех тестов"""
        question = "Какой язык?"
        self.my_survey = AnS(question)
        self.responses = ['английский', 'русский', 'китайский']

    def test_store_single_response(self):
        """проверяет что один ответ принят правильно"""
        self.my_survey.store_response(self.responses[0])
        self.assertIn(self.responses[0], self.my_survey.responses)

    def test_store_three_response(self):
        """проверяет что три ответа приняты правильно"""
        [self.my_survey.store_response(each) for each in self.responses]
        [self.assertIn(each, self.my_survey.responses) for each in self.responses]


if __name__ == '__main__':
    unittest.main()
