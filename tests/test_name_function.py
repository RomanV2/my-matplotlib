import unittest

from name_function import get_formatted_name

class NamesTestCase(unittest.TestCase):
    """
    тесты для 'name_function.py'
    """

    def test_first_last_name(self):
        """
        имена вида 'Джонни Деп' работают правильно?
        """

        formatted_name = get_formatted_name('джонни', 'деп')
        self.assertEqual(formatted_name, 'Джонни Деп')

    def test_first_middle_last_name(self):
        """
        имена вида 'василий петрович пупкин' работают правильно?
        """
        formatted_name = get_formatted_name('василий', 'пупкин', 'петрович')
        self.assertEqual(formatted_name, 'Василий Петрович Пупкин')


if __name__ == '__main__':
    unittest.main()
