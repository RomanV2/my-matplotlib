from name_function import get_formatted_name

print('"q" для выхода')
while True:
    first = input("\nВведите Имя: ")
    if first == 'q':
        break
    last = input("\nВведите фамилию: ")
    if last == 'q':
        break

    formatted_name = get_formatted_name(first, last)
    print(f'Полное Имя: {formatted_name}')
