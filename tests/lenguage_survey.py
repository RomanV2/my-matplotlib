from survey import AnonymousSurvey as AnS

# определение вопроса
question = 'Какой язык ты изучаешь?'

# создание экземпляра
my_survey = AnS(question)

# вывод вопроса
my_survey.show_question()

# цикл приема вопросов
while True:
    print('для выхода "q"')
    response = input('язык: ')
    if response == 'q':
        break
    my_survey.store_response(response)

# вывод результатов опроса
my_survey.show_result()
