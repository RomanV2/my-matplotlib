import unittest

from city_functions import get_city_country_population


class CityCountryCase(unittest.TestCase):
    """
    тесты для 'city_functions.py'
    """

    def test_city_country_population(self):
        formatted = get_city_country_population('окланд', 'новая зеландия', 5_000_000)
        self.assertEqual(formatted, "Окланд, Новая Зеландия - Population 5000000")

    def test_city_country(self):
        formatted = get_city_country_population('окланд', 'новая зеландия')
        self.assertEqual(formatted, "Окланд, Новая Зеландия - Population 0")


if __name__ == '__main__':
    unittest.main()
