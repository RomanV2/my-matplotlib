class AnonymousSurvey:
    """сбор анонимных ответов"""
    def __init__(self, question):
        """сохраняет вопрос и создает место храниения ответов"""
        self.question = question
        self.responses = []

    def show_question(self):
        """выводит вопрос"""
        print(self.question)

    def store_response(self, new_response):
        """сохраняет один ответ"""
        self.responses.append(new_response)

    def show_result(self):
        """выводит результаты"""
        print('Результаты:')
        [print(f' - {response}') for response in self.responses]

