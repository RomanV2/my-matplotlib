def get_city_country_population(
        city: str, country: str, population: int = 0) -> str:
    """
    выдает отформатированную строку города и страны
    :param str city: принимает город
    :param str country: принимает страну
    :param int population: принимает количество наслеления
    :return str: возвращает отформатированную строку
    """

    formatted_city_country_pop = f'{city}, {country} - population {population}'
    return formatted_city_country_pop.title()

