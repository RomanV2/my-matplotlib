class Employee:
    """класс сотрудника"""
    def __init__(self, salary=100_000):
        self.salary = salary

    def show_salary(self):
        """показ зарплаты"""
        return self.salary

    def give_raise(self, increase=20_000):
        """увеличение зарплаты"""
        self.salary += increase
