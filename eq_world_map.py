import json

from plotly.graph_objs import Scattergeo, Layout
from plotly import offline


# изучение структуры данных
filename = 'data/1.0_month.geojson.json'
with open(filename) as f:
    all_eq_data = json.load(f)
    all_eq_dicts = all_eq_data['features']
    # хранаение магнитуд
    mags, lons, lats, hover_texts = [], [], [], []
    for eq_dict in all_eq_dicts:
        mag = eq_dict['properties']['mag']
        title = eq_dict['properties']['title']
        lon = eq_dict['geometry']['coordinates'][0]
        lat = eq_dict['geometry']['coordinates'][1]
        mags.append(mag)
        hover_texts.append(title)
        lons.append(lon)
        lats.append(lat)

# нанесение данных на карту
data = [{
    'type': 'scattergeo',
    'lon': lons,
    'lat': lats,
    'text': hover_texts,
    'marker': {
        'size': [5*mag for mag in mags],
        'color': mags,
        'colorscale': 'Hot',
        'reversescale': True,
        'colorbar': {'title': 'Магнитуда'}
    },
}]
my_layout = Layout(title='Землетрясения')
fig = {'data': data, 'layout': my_layout}
offline.plot(fig, filename='data/global_earthquakes.html')
