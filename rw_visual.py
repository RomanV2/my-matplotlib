import matplotlib.pyplot as plt

from random_walk import RandomWalk

while True:
    # построение случаного блуждания и нанесение точек на диаграмму
    rw = RandomWalk(50000)
    rw.fill_walk()

    # назначение области простмотра
    plt.figure(dpi=128, figsize=(10, 6))

    # вывод точек и отображение диаграммы
    points_number = list(range(rw.num_points))
    plt.scatter(rw.x_values, rw.y_values, c=points_number, cmap=plt.cm.Blues, edgecolors='none', s=2)
    plt.scatter(0, 0, c='green', edgecolors='none', s=50)
    plt.scatter(rw.x_values[-1], rw.y_values[-1], c='red', edgecolors='none', s=100)

    # удаление осей
    plt.axis('off')

    plt.show()

    keep_running = input('добавить еще одну точнку? (y/n): ')
    if keep_running == 'n':
        break
