import pygal


# создаем экземпляр класса World
wm = pygal.maps.world.World()
wm.title = 'Население Северной Америки'

wm.add('Северная Америка', {'ca': 3421000, 'mx': 30999910, 'us': 14354464})

wm.render_to_file('result/na_populations.svg')
