import pygal
from typing import List

from die import Die


# создание экземпляра кубика D6
die = Die()

# моделирование серии бросков кубика
results: List[int] = []
for roll_num in range(1000):
    result = die.roll()
    results.append(result)

# анализ результатов
frequencies: List[int] = []
for value in range(1, die.num_sides+1):
    frequency = results.count(value)
    frequencies.append(frequency)

# визуализация результатов
hist = pygal.Bar()

hist.title = "результат 1000 бросков кубика D6"
hist.x_labels = ['1', '2', '3', '4', '5', '6']
hist._x_title = "Результат"
hist._y_title = "Частота результата"

hist.add('D6', frequencies)
hist.render_to_file('die_visual.svg')