import pygal

from typing import List
from die import Die

# создание кубика D6 и кубика D10
die_1 = Die()
die_2 = Die(10)

# моделирование серии бросков с сохранением результата в списке
results = []
for roll_num in range(50_000):
    result = die_1.roll() + die_2.roll()
    results.append(result)

# анализ результатов
frequencies: List[int] = []
max_result = die_1.num_sides + die_2.num_sides
for value in range(2, max_result+1):
    frequency = results.count(value)
    frequencies.append(frequency)

# визуализация результатов
hist = pygal.Bar()

hist.title = "Результат 50 000 бросков кубика D6 и D10"
hist.x_labels = list(map(str, range(2, 17)))
hist.x_title = "Результат"
hist.y_title = "Частота результата"

hist.add('D6 + D10', frequencies)
hist.render_to_file('diff_dice_visual.svg')

