from pygal_maps_world import i18n


def get_country_code(country_name: str) -> str or None:
    """
    возвращает для заданной страны ее код из 2х букв
    :param country_name:
    :return: str or None
    """
    for code, name in i18n.COUNTRIES.items():
        if name == country_name:
            return code
    # если страна не найдена, вернуть None
    return None


print(get_country_code('Andorra'))
print(get_country_code('United Arab Emirates'))
print(get_country_code('Afghanistan'))
