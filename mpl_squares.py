import matplotlib
import matplotlib.pyplot as plt

matplotlib.use('TkAgg')
x_values = range(6)
y_values = [i**2 for i in range(6)]
plt.plot(x_values, y_values, c=(0, 0, 0.9), linewidth=2)

# назначение заголовка диаграмы и меток осей
plt.title('Квадрат чисел', fontsize=24)
plt.xlabel('Значение', fontsize=12)
plt.ylabel('Квадрат значения', fontsize=12)

# назначение размера шрифта делений на осях
plt.tick_params(axis='both', which='major', labelsize=12)

plt.scatter(x_values, y_values, c=y_values, cmap=plt.cm.Blues, edgecolors='blue', s=200)

plt.show()

# для сохранения использовать команду
# plt.savefig('test', bbox_inches='tight')
